﻿Type=StaticCode
Version=5.9
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
'Static code module
Sub Process_Globals
    Dim Const X_COORD = 0 As Int
    Dim Const Y_COORD = 1 As Int
    Dim Const O_ANGLE = 2 As Int
    Dim const HALF_PI = cPI / 2 As Double

    Dim cvs As Canvas

    Dim lstAngles   As List
    Dim lstPoints   As List
    Dim angleBuffer As List

    Dim pMap        As Map
    Dim pFck        As Map
    Dim oX, oY      As Double

    Dim gMinX       As Double
    Dim gMinY       As Double
    Dim gMaxX       As Double
    Dim gMaxY       As Double

    Dim ts As Long

    Type PointAngle(x As Double, y As Double, a As Double)
End Sub

Sub Initialize(canvas As Canvas)
    cvs = canvas
    lstAngles.Initialize
    lstPoints.Initialize
    angleBuffer.initialize
    pMap.Initialize
    pFck.Initialize
End Sub

Sub Cast(matrix(,) As Int, matrixSizeX As Int, matrixSizeY As Int, gridStep As Double, left As Double, top As Double, originX As Double, originY As Double, mX As Int, mY As Int)
    Dim fx As JFX

    ts = DateTime.now

    oX = originX - left
    oY = originY - top

    lstAngles.Clear
    lstPoints.Clear
    angleBuffer.Clear

    pMap.Clear
    pFck.Clear

    Dim iter   = 0                                                  As Int
    Dim count  = matrixSizeX * matrixSizeY                          As Int
    Dim moves  = 1                                                  As Int
    Dim xdir() = Array As Short( 0,  1,  0, -1)                     As Short
    Dim ydir() = Array As Short(-1,  0,  1,  0)                     As Short
    Dim angle  = 0                                                  As Short
    Dim mX     = Floor(Max(Min(oX / gridStep, matrixSizeX - 1), 0)) As Int
    Dim mY     = Floor(Max(Min(oY / gridStep, matrixSizeY - 1), 0)) As Int

    Dim exitFlag = False As Boolean
    Dim north, south, east, west As Double

    Do While count > 0
        iter = moves
        Do While iter > 0
            If lstAngles.Size == 1 Then
                Dim singleEntry() = lstAngles.Get(0) As Double
                If singleEntry(0) == -cPI And singleEntry(1) == cPI Then
                    exitFlag = True
                    Exit
                End If
            End If

            '--------------------------------------------------------------------------
            iter = iter - 1
            mX = mX + xdir(angle)
            mY = mY + ydir(angle)
            If mX < 0 Or mX >= matrixSizeX Or mY < 0 Or mY >= matrixSizeY Or matrix(mX, mY) < 1 Then Continue
            '--------------------------------------------------------------------------

            west  = mX    * gridStep
            east  = west  + gridStep
            north = mY    * gridStep
            south = north + gridStep

            If oX >= west And oX <= east Then
                If oY < north Then
                    remPoint(east, south)
                    remPoint(west, south)
                    addPoint(east, north)
                    addPoint(west, north)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                Else If oY > south Then
                    remPoint(west, north)
                    remPoint(east, north)
                    addPoint(west, south)
                    addPoint(east, south)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                End If
            Else If oY >= north And oY <= south Then
                If oX > east Then
                    remPoint(west, south)
                    remPoint(west, north)
                    addPoint(east, south)
                    addPoint(east, north)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                Else If oX < west Then
                    remPoint(east, north)
                    remPoint(east, south)
                    addPoint(west, north)
                    addPoint(west, south)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                End If
            Else If oX < west Then
                If oY < north Then
                    remPoint(east, south)
                    addPoint(east, north)
                    addPoint(west, north)
                    addPoint(west, south)
                    AddRange(lstAngles, angleBuffer.Get(1), angleBuffer.Get(2), -cPI, cPI)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                Else
                    remPoint(east, north)
                    addPoint(west, north)
                    addPoint(west, south)
                    addPoint(east, south)
                    AddRange(lstAngles, angleBuffer.Get(1), angleBuffer.Get(2), -cPI, cPI)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                End If
            Else If oX > east Then
                If oY < north Then
                    remPoint(west, south)
                    addPoint(east, south)
                    addPoint(east, north)
                    addPoint(west, north)
                    AddRange(lstAngles, angleBuffer.Get(1), angleBuffer.Get(2), -cPI, cPI)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                Else
                    remPoint(west, north)
                    addPoint(west, south)
                    addPoint(east, south)
                    addPoint(east, north)
                    AddRange(lstAngles, angleBuffer.Get(1), angleBuffer.Get(2), -cPI, cPI)
                    AddRange(lstAngles, angleBuffer.Get(0), angleBuffer.Get(1), -cPI, cPI)
                End If
            End If
            angleBuffer.Clear
        Loop
        If exitFlag Then Exit
        '--------------------
        angle = (angle + 1) Mod 4
        moves = moves + Floor(1 - angle Mod 2)
        count = count - 1
    Loop

    gMinX = 0
    gMinY = 0
    gMaxX = matrixSizeX * gridStep
    gMaxY = matrixSizeY * gridStep

    addPoint(gMinX, gMaxY)
    addPoint(gMinX, gMinY)
    addPoint(gMaxX, gMinY)
    addPoint(gMaxX, gMaxY)

    'Remove unnecessary points
    For Each key In pFck.Keys
        pMap.Remove(key)
    Next


    ' - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    'Prepare variables (outer loop)
    Dim xx, yy As Int
    Dim x, y, a As Double
    Dim cosA, sinA, cosP, sinP As Double
    Dim rangeDemultiplier = 0.000001, thisAngle As Double
    Dim raySize = gridStep * (matrixSizeX + matrixSizeY) As Int

    'Prepare variables (inner loop)
    Dim xya()     As Double
    Dim poi()     As Double
    Dim perp      As Double
    Dim cosP      As Double
    Dim sinP      As Double
    Dim xxP1      As Double
    Dim yyP1      As Double
    Dim xxP2      As Double
    Dim yyP2      As Double
    Dim rX        As Double
    Dim rY        As Double
    Dim dst       As Double
    Dim new_x     As Double
    Dim new_y     As Double
    Dim poiList   As List
    Dim blockList As Map

    'Check if the ray should be extended to POI
    poiList.Initialize
    For Each key In pMap.Keys
        xya = pMap.Get(key)
        x   = xya(X_COORD)
        y   = xya(Y_COORD)
        a   = xya(O_ANGLE)

        For signMultiplier = -1 To 1
            thisAngle = a + (signMultiplier * rangeDemultiplier)
            cosA = Cos(thisAngle) : xx = (x + cosA) / gridStep
            sinA = Sin(thisAngle) : yy = (y + sinA) / gridStep
            If xx < 0 Or xx >= matrixSizeX Or yy < 0 Or yy >= matrixSizeY Or matrix(xx, yy) == 0 Then
                perp = a + HALF_PI
                cosP = Cos(perp)
                sinP = Sin(perp)
                xxP1 = (x + cosP) / gridStep
                yyP1 = (y + sinP) / gridStep
                xxP2 = (x - cosP) / gridStep
                yyP2 = (y - sinP) / gridStep
                If False == ( _
                     (xxP1 >= 0 And xxP1 < matrixSizeX And yyP1 >= 0 And yyP1 < matrixSizeY And matrix(xxP1, yyP1) > 0) And _
                      (xxP2 >= 0 And xxP2 < matrixSizeX And yyP2 >= 0 And yyP2 < matrixSizeY And matrix(xxP2, yyP2) > 0) _
                ) Then
                    rX = x + cosA * raySize
                    rY = y + sinA * raySize
                    blockList = getBlocks(x, y, rX, rY, gridStep, matrixSizeX, matrixSizeY)

                    poiList.Clear
                    For Each newBlk() As Double In blockList.Values
                        Dim nbX = newBlk(X_COORD) As Int
                        Dim nbY = newBlk(Y_COORD) As Int
                        If nbX < 0 Or nbX >= matrixSizeX Or nbY < 0 Or nbY >= matrixSizeY Then Continue
                        '------------------------------------------------------------------------------

                        'TODO: Check which walls should be tested
                        west  = (nbX   * gridStep)
                        east  = (west  + gridStep)
                        north = (nbY   * gridStep)
                        south = (north + gridStep)

                        'Handle corner cases
                        If nbY - 1 > 0 And matrix(nbX, nbY - 1) > 0 Then
                            poi = getIntersection(oX, oY, cosA, sinA, east, north, west, north)
                            If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                        End If
                        If nbY + 1 < matrixSizeY And matrix(nbX, nbY + 1) > 0 Then
                            poi = getIntersection(oX, oY, cosA, sinA, east, south, west, south)
                            If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                        End If
                        If nbX - 1 > 0 And matrix(nbX - 1, nbY) > 0 Then
                            poi = getIntersection(oX, oY, cosA, sinA, west, north, west, south)
                            If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                        End If
                        If nbX + 1 < matrixSizeX And matrix(nbX + 1, nbY) > 0 Then
                            poi = getIntersection(oX, oY, cosA, sinA, east, north, east, south)
                            If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                        End If

                        If matrix(nbX, nbY) < 1 Then Continue
                        '--------------------------------------------------------------------------------------

                        'Get points of intersection
                        If oX >= west And oX <= east Then
                            If oY < north Then
                                poi = getIntersection(oX, oY, cosA, sinA, east, north, west, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            Else If oY > south Then
                                poi = getIntersection(oX, oY, cosA, sinA, east, south, west, south)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            End If
                        Else If oY >= north And oY <= south Then
                            If oX > east Then
                                poi = getIntersection(oX, oY, cosA, sinA, east, south, east, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            Else If oX < west Then
                                poi = getIntersection(oX, oY, cosA, sinA, west, south, west, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            End If
                        Else If oX < west Then
                            If oY < north Then
                                poi = getIntersection(oX, oY, cosA, sinA, east, north, west, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                                poi = getIntersection(oX, oY, cosA, sinA, west, south, west, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            Else
                                poi = getIntersection(oX, oY, cosA, sinA, west, south, west, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                                poi = getIntersection(oX, oY, cosA, sinA, east, south, west, south)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            End If
                        Else If oX > east Then
                            If oY < north Then
                                poi = getIntersection(oX, oY, cosA, sinA, east, south, east, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                                poi = getIntersection(oX, oY, cosA, sinA, east, north, west, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            Else
                                poi = getIntersection(oX, oY, cosA, sinA, east, south, west, south)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                                poi = getIntersection(oX, oY, cosA, sinA, east, south, east, north)
                                If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                            End If
                        End If
                    Next

                    'Get points of intersection (egdes)
                    poi = getIntersection(oX, oY, cosA, sinA, gMinX, gMinY, gMinX, gMaxY)
                    If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                    poi = getIntersection(oX, oY, cosA, sinA, gMinX, gMaxY, gMaxX, gMaxY)
                    If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                    poi = getIntersection(oX, oY, cosA, sinA, gMaxX, gMaxY, gMaxX, gMinY)
                    If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))
                    poi = getIntersection(oX, oY, cosA, sinA, gMaxX, gMinY, gMinX, gMinY)
                    If poi <> Null Then poiList.Add(getDistance(oX, oY, poi(0), poi(1)))

                    'Get the closest point of intersection
                    If poiList.Size > 0 Then
                        poiList.Sort(True)
                        dst = poiList.Get(0)
                        new_x = oX + dst * cosA
                        new_y = oY + dst * sinA
                        pMap.Put(xyKey(new_x, new_y), Array As Double(new_x, new_y, a))
                    End If

                End If
            End If
        Next
    Next

    'TODO: Sort by angle
    For Each key In pMap.Keys
        xya = pMap.Get(key)
        x   = xya(X_COORD)
        y   = xya(Y_COORD)
        cvs.DrawLine(left + oX, top + oY, left + x, top + y, Main.GetColor(2), 1)
        cvs.DrawCircle( _
            left + x, top  + y, _
            2, fx.Colors.Yellow, False, 1)
    Next

    cvs.DrawText("Raycasting done: " & (DateTime.Now - ts) & " ms", 10, 20, fx.DefaultFont(12), fx.Colors.Yellow, "LEFT")

End Sub

Sub getDistance(x1 As Double, y1 As Double, x2 As Double, y2 As Double) As Double
    Dim dx = x2 - x1 As Double
    Dim dy = y2 - y1 As Double
    Return Sqrt((dx * dx) + (dy * dy))
End Sub

Sub getIntersection(x As Double, y As Double, dx As Double, dy As Double, x1 As Double, y1 As Double, x2 As Double, y2 As Double) As Double()
    Dim r, s, d As Double
    If (dy / dx <> (y2 - y1) / (x2 - x1)) Then
        d = ((dx * (y2 - y1)) - dy * (x2 - x1))
        If (d <> 0) Then
            r = (((y - y1) * (x2 - x1)) - (x - x1) * (y2 - y1)) / d
            s = (((y - y1) * dx) - (x - x1) * dy) / d
            If (r >= 0 And s >= 0 And s <= 1) Then Return Array As Double(x + r * dx, y + r * dy)
        End If
    End If
    Return Null
End Sub

Sub calcAngle(x As Double, y As Double) As Double
    Return ATan2(y - oY, x - oX)
End Sub

Sub xyKey(x As Double, y As Double) As String
    Return x & "," & y
End Sub

Sub addPoint(x As Double, y As Double)
    Dim pAngle = calcAngle(x, y) As Double
    Dim key = xyKey(x, y) As String
    If isNewAngle(pAngle) Then
        pMap.Put( _
            key, Array As Double(x, y, pAngle) _
        )
    End If
    angleBuffer.Add(pAngle)
End Sub

Sub remPoint(x As Double, y As Double)
    pFck.Put(xyKey(x, y), Null)
End Sub

Sub isNewAngle(a As Double) As Boolean
    For Each range() As Double In lstAngles
        If a >= range(0) And a <= range(1) Then    Return False
    Next
    Return True
End Sub

Sub getBlocks(x1 As Double, y1 As Double, x2 As Double, y2 As Double, blockSize As Double, maxX As Int, maxY As Int) As Map
    Dim stepX, stepY   As Double
    Dim dX = (x2 - x1) As Double
    Dim dY = (y2 - y1) As Double

    If (x1 = x2) And (y1 = y2) Then
        stepX = 0
        stepY = 0
        'Log("It's a point!")
    Else if (x1 = x2) And (y1 <> y2) Then
        stepX = 0
        stepY = blockSize
        'Log("It's a vertical line!")
    Else if (x1 <> x2) And (y1 = y2) Then
        stepX = blockSize
        stepY = 0
        'Log("It's a horizontal line!")
    Else
        Dim absDx = Abs(dX) As Double
        Dim absDy = Abs(dY) As Double
        If absDx > absDy Then
            stepX = blockSize
            stepY = (absDy / absDx) * blockSize
        Else
            stepX = (absDx / absDy) * blockSize
            stepY = blockSize
        End If
    End If
    If dX < 0 Then stepX = -stepX
    If dY < 0 Then stepY = -stepY

    stepX = stepX / 10
    stepY = stepY / 10

    Dim pX = x1 As Double
    Dim pY = y1 As Double
    Dim bX, bY As Int
    Dim blocks As Map : blocks.Initialize
    Do Until Abs(pX - x1) > Abs(dX) Or Abs(pY - y1) > Abs(dY)
        bX = Floor(pX / blockSize)
        bY = Floor(pY / blockSize)
        If bX < 0 Or bX >= maxX Or bY < 0 Or bY >= maxY Then Exit
        blocks.put(bX & ":" & bY, Array As Double(bX, bY))
        pX = pX + stepX
        pY = pY + stepY
    Loop
    bX = Floor(x2 / blockSize)
    bY = Floor(y2 / blockSize)
    blocks.put(bX & ":" & bY, Array As Double(bX, bY))
    Return blocks
End Sub

Sub AddRange(lst As List, a As Double, b As Double, lowest As Double, highest As Double) As Boolean
    If a > highest Or b > highest Or a < lowest Or b < lowest Then Return False
    '--------------------------------------------------------------------------
    Dim i As Int
    If a > b Then
        i = lst.Size - 1
        Do While i >= 0
            Dim range() = lst.Get(i) As Double
            If (a >= range(0) And a <= range(1)) Or (range(0) >= a And range(0) <= highest) Then
                a = Min(a, range(0))
                lst.RemoveAt(i)
            End If
            i = i - 1
        Loop
        lst.Add(Array As Double(a, highest))
        '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        i = lst.Size - 1
        Do While i >= 0
            Dim range() = lst.Get(i) As Double
            If (b >= range(0) And b <= range(1)) Or (range(1) >= lowest And range(1) <= b) Then
                b = Max(b, range(1))
                lst.RemoveAt(i)
            End If
            i = i - 1
        Loop
        lst.Add(Array As Double(lowest, b))
        '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    Else
        i = lst.Size - 1
        Do While i >= 0
            Dim range() = lst.Get(i) As Double
            If (a >= range(0) And a <= range(1)) Or (b >= range(0) And b <= range(1)) _
            Or (range(0) >= a And range(0) <= b) Or (range(1) >= a And range(1) <= b) Then
                a = Min(a, range(0))
                b = Max(b, range(1))
                lst.RemoveAt(i)
            End If
            i = i - 1
        Loop
        lst.Add(Array As Double(a, b))
    End If
    Return True
End Sub