package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.debug.*;

public class main extends javafx.application.Application{
public static main mostCurrent = new main();

public static BA ba;
static {
		ba = new  anywheresoftware.b4j.objects.FxBA("b4j.example", "b4j.example.main", null);
		ba.loadHtSubs(main.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.main", ba);
		}
	}
    public static Class<?> getObject() {
		return main.class;
	}

 
    public static void main(String[] args) {
    	launch(args);
    }
    public void start (javafx.stage.Stage stage) {
        try {
            if (!false)
                System.setProperty("prism.lcdtext", "false");
            anywheresoftware.b4j.objects.FxBA.application = this;
		    anywheresoftware.b4a.keywords.Common.setDensity(javafx.stage.Screen.getPrimary().getDpi());
            anywheresoftware.b4a.keywords.Common.LogDebug("Program started.");
            initializeProcessGlobals();
            anywheresoftware.b4j.objects.Form frm = new anywheresoftware.b4j.objects.Form();
            frm.initWithStage(ba, stage, 800, 800);
            ba.raiseEvent(null, "appstart", frm, (String[])getParameters().getRaw().toArray(new String[0]));
        } catch (Throwable t) {
            BA.printException(t, true);
            System.exit(1);
        }
    }
public static anywheresoftware.b4a.keywords.Common __c = null;
public static anywheresoftware.b4j.objects.JFX _vvvvvv3 = null;
public static anywheresoftware.b4j.objects.Form _vvvvv7 = null;
public static anywheresoftware.b4j.objects.CanvasWrapper _v5 = null;
public static double _vvvvv0 = 0;
public static double _vvvvvv1 = 0;
public static b4j.example.main._grid _vvvvvv5 = null;
public static anywheresoftware.b4a.objects.Timer _vvvvvv4 = null;
public static anywheresoftware.b4a.objects.collections.Map _vvvvvv2 = null;
public static boolean _vvvvvv0 = false;
public static double _vv3 = 0;
public static double _vv4 = 0;
public static int _vvvvvv6 = 0;
public static int _vvvvvv7 = 0;
public static b4j.example.raycaster _vvvvvvv1 = null;
public static class _grid{
public boolean IsInitialized;
public double gridStep;
public int sizeX;
public int sizeY;
public double width;
public double height;
public double left;
public double right;
public double top;
public double btm;
public int[][] matrix;
public void Initialize() {
IsInitialized = true;
gridStep = 0;
sizeX = 0;
sizeY = 0;
width = 0;
height = 0;
left = 0;
right = 0;
top = 0;
btm = 0;
matrix = new int[0][];
{
int d0 = matrix.length;
int d1 = 0;
for (int i0 = 0;i0 < d0;i0++) {
matrix[i0] = new int[d1];
}
}
;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static class _point{
public boolean IsInitialized;
public double x;
public double y;
public double a;
public void Initialize() {
IsInitialized = true;
x = 0;
y = 0;
a = 0;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static String  _appstart(anywheresoftware.b4j.objects.Form _form1,String[] _args) throws Exception{
 //BA.debugLineNum = 35;BA.debugLine="Sub AppStart (Form1 As Form, Args() As String)";
 //BA.debugLineNum = 36;BA.debugLine="MainForm = Form1";
_vvvvv7 = _form1;
 //BA.debugLineNum = 37;BA.debugLine="MainForm.Resizable = False";
_vvvvv7.setResizable(anywheresoftware.b4a.keywords.Common.False);
 //BA.debugLineNum = 38;BA.debugLine="MainForm.Show";
_vvvvv7.Show();
 //BA.debugLineNum = 40;BA.debugLine="cvs.Initialize(\"cvs\")";
_v5.Initialize(ba,"cvs");
 //BA.debugLineNum = 41;BA.debugLine="MainForm.RootPane.AddNode(cvs, 0, 0, MainF";
_vvvvv7.getRootPane().AddNode((javafx.scene.Node)(_v5.getObject()),0,0,_vvvvv7.getWidth(),_vvvvv7.getHeight());
 //BA.debugLineNum = 42;BA.debugLine="cvsCenterX = cvs.Width / 2";
_vvvvv0 = _v5.getWidth()/(double)2;
 //BA.debugLineNum = 43;BA.debugLine="cvsCenterY = cvs.Height / 2";
_vvvvvv1 = _v5.getHeight()/(double)2;
 //BA.debugLineNum = 45;BA.debugLine="colorMap.Initialize";
_vvvvvv2.Initialize();
 //BA.debugLineNum = 46;BA.debugLine="colorMap.Put(1, fx.Colors.Gray)";
_vvvvvv2.Put((Object)(1),(Object)(_vvvvvv3.Colors.Gray));
 //BA.debugLineNum = 47;BA.debugLine="colorMap.Put(2, fx.Colors.Green)";
_vvvvvv2.Put((Object)(2),(Object)(_vvvvvv3.Colors.Green));
 //BA.debugLineNum = 48;BA.debugLine="colorMap.Put(3, fx.Colors.Blue)";
_vvvvvv2.Put((Object)(3),(Object)(_vvvvvv3.Colors.Blue));
 //BA.debugLineNum = 49;BA.debugLine="colorMap.Put(4, fx.Colors.Red)";
_vvvvvv2.Put((Object)(4),(Object)(_vvvvvv3.Colors.Red));
 //BA.debugLineNum = 50;BA.debugLine="colorMap.Put(5, fx.Colors.Yellow)";
_vvvvvv2.Put((Object)(5),(Object)(_vvvvvv3.Colors.Yellow));
 //BA.debugLineNum = 52;BA.debugLine="InitCode";
_vvvvv2();
 //BA.debugLineNum = 54;BA.debugLine="MainLoop.Initialize(\"MainLoop\", 16)";
_vvvvvv4.Initialize(ba,"MainLoop",(long) (16));
 //BA.debugLineNum = 55;BA.debugLine="MainLoop.Enabled = True";
_vvvvvv4.setEnabled(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 57;BA.debugLine="End Sub";
return "";
}
public static String  _vvvv4() throws Exception{
 //BA.debugLineNum = 157;BA.debugLine="Sub ClearCanvas";
 //BA.debugLineNum = 158;BA.debugLine="cvs.ClearRect(0, 0, cvs.Width, cvs.Height)";
_v5.ClearRect(0,0,_v5.getWidth(),_v5.getHeight());
 //BA.debugLineNum = 159;BA.debugLine="cvs.DrawRect(0, 0, cvs.Width, cvs.Height,";
_v5.DrawRect(0,0,_v5.getWidth(),_v5.getHeight(),_vvvvvv3.Colors.Black,anywheresoftware.b4a.keywords.Common.True,0);
 //BA.debugLineNum = 160;BA.debugLine="End Sub";
return "";
}
public static String  _cvs_mousedragged(anywheresoftware.b4j.objects.NodeWrapper.MouseEventWrapper _eventdata) throws Exception{
b4j.example.main._point _p = null;
 //BA.debugLineNum = 192;BA.debugLine="Sub cvs_MouseDragged(eventData As MouseEvent)";
 //BA.debugLineNum = 193;BA.debugLine="Dim p = PixelToGrid(eventData.X, eventData";
_p = _vvvvv4(_eventdata.getX(),_eventdata.getY());
 //BA.debugLineNum = 194;BA.debugLine="If eventData.PrimaryButtonDown Then";
if (_eventdata.getPrimaryButtonDown()) { 
 //BA.debugLineNum = 195;BA.debugLine="testGrid.matrix(p.x, p.y) = 1";
_vvvvvv5.matrix[(int) (_p.x)][(int) (_p.y)] = (int) (1);
 }else {
 //BA.debugLineNum = 197;BA.debugLine="oX = eventData.X";
_vv3 = _eventdata.getX();
 //BA.debugLineNum = 198;BA.debugLine="oY = eventData.Y";
_vv4 = _eventdata.getY();
 //BA.debugLineNum = 199;BA.debugLine="mX = p.x";
_vvvvvv6 = (int) (_p.x);
 //BA.debugLineNum = 200;BA.debugLine="mY = p.y";
_vvvvvv7 = (int) (_p.y);
 };
 //BA.debugLineNum = 202;BA.debugLine="End Sub";
return "";
}
public static String  _cvs_mousepressed(anywheresoftware.b4j.objects.NodeWrapper.MouseEventWrapper _eventdata) throws Exception{
b4j.example.main._point _p = null;
 //BA.debugLineNum = 178;BA.debugLine="Sub cvs_MousePressed(eventData As MouseEvent)";
 //BA.debugLineNum = 179;BA.debugLine="Dim p = PixelToGrid(eventData.X, eventData";
_p = _vvvvv4(_eventdata.getX(),_eventdata.getY());
 //BA.debugLineNum = 180;BA.debugLine="Log(p.x & \", \" & p.y)";
anywheresoftware.b4a.keywords.Common.Log(BA.NumberToString(_p.x)+", "+BA.NumberToString(_p.y));
 //BA.debugLineNum = 181;BA.debugLine="If eventData.PrimaryButtonDown Then";
if (_eventdata.getPrimaryButtonDown()) { 
 //BA.debugLineNum = 182;BA.debugLine="testGrid.matrix(p.x, p.y) = 1";
_vvvvvv5.matrix[(int) (_p.x)][(int) (_p.y)] = (int) (1);
 }else {
 //BA.debugLineNum = 184;BA.debugLine="oX = eventData.X";
_vv3 = _eventdata.getX();
 //BA.debugLineNum = 185;BA.debugLine="oY = eventData.Y";
_vv4 = _eventdata.getY();
 //BA.debugLineNum = 186;BA.debugLine="mX = p.x";
_vvvvvv6 = (int) (_p.x);
 //BA.debugLineNum = 187;BA.debugLine="mY = p.y";
_vvvvvv7 = (int) (_p.y);
 //BA.debugLineNum = 188;BA.debugLine="justDoIt = True";
_vvvvvv0 = anywheresoftware.b4a.keywords.Common.True;
 };
 //BA.debugLineNum = 190;BA.debugLine="End Sub";
return "";
}
public static String  _vvvv5() throws Exception{
 //BA.debugLineNum = 137;BA.debugLine="Sub DrawAll";
 //BA.debugLineNum = 138;BA.debugLine="ClearCanvas";
_vvvv4();
 //BA.debugLineNum = 139;BA.debugLine="DrawCells";
_vvvv6();
 //BA.debugLineNum = 140;BA.debugLine="DrawGrid";
_vvvv7();
 //BA.debugLineNum = 141;BA.debugLine="If justDoIt Then RayCaster.Cast(testGrid.m";
if (_vvvvvv0) { 
_vvvvvvv1._vvv5(_vvvvvv5.matrix,_vvvvvv5.sizeX,_vvvvvv5.sizeY,_vvvvvv5.gridStep,_vvvvvv5.left,_vvvvvv5.top,_vv3,_vv4,_vvvvvv6,_vvvvvv7);};
 //BA.debugLineNum = 142;BA.debugLine="End Sub";
return "";
}
public static String  _vvvv6() throws Exception{
double _gx = 0;
double _gy = 0;
int _x = 0;
int _y = 0;
 //BA.debugLineNum = 144;BA.debugLine="Sub DrawCells";
 //BA.debugLineNum = 145;BA.debugLine="Dim gX, gY As Double";
_gx = 0;
_gy = 0;
 //BA.debugLineNum = 146;BA.debugLine="For x = 0 To testGrid.sizeX - 1";
{
final int step2 = 1;
final int limit2 = (int) (_vvvvvv5.sizeX-1);
_x = (int) (0) ;
for (;(step2 > 0 && _x <= limit2) || (step2 < 0 && _x >= limit2) ;_x = ((int)(0 + _x + step2))  ) {
 //BA.debugLineNum = 147;BA.debugLine="gX = testGrid.left + (x * testGrid.gri";
_gx = _vvvvvv5.left+(_x*_vvvvvv5.gridStep);
 //BA.debugLineNum = 148;BA.debugLine="For y = 0 To testGrid.sizeY - 1";
{
final int step4 = 1;
final int limit4 = (int) (_vvvvvv5.sizeY-1);
_y = (int) (0) ;
for (;(step4 > 0 && _y <= limit4) || (step4 < 0 && _y >= limit4) ;_y = ((int)(0 + _y + step4))  ) {
 //BA.debugLineNum = 149;BA.debugLine="gY = testGrid.top + (y * testGrid.";
_gy = _vvvvvv5.top+(_y*_vvvvvv5.gridStep);
 //BA.debugLineNum = 150;BA.debugLine="If testGrid.matrix(x, y) > 0 Then";
if (_vvvvvv5.matrix[_x][_y]>0) { 
 //BA.debugLineNum = 151;BA.debugLine="cvs.DrawRect(gX, gY, testGrid.";
_v5.DrawRect(_gx,_gy,_vvvvvv5.gridStep,_vvvvvv5.gridStep,(javafx.scene.paint.Paint)(_vvvvvv2.Get((Object)(_vvvvvv5.matrix[_x][_y]))),anywheresoftware.b4a.keywords.Common.True,0);
 };
 }
};
 }
};
 //BA.debugLineNum = 155;BA.debugLine="End Sub";
return "";
}
public static String  _vvvv7() throws Exception{
int _x = 0;
int _y = 0;
 //BA.debugLineNum = 162;BA.debugLine="Sub DrawGrid";
 //BA.debugLineNum = 163;BA.debugLine="For x = testGrid.left To (testGrid.left +";
{
final int step1 = (int) (_vvvvvv5.gridStep);
final int limit1 = (int) ((_vvvvvv5.left+_vvvvvv5.width));
_x = (int) (_vvvvvv5.left) ;
for (;(step1 > 0 && _x <= limit1) || (step1 < 0 && _x >= limit1) ;_x = ((int)(0 + _x + step1))  ) {
 //BA.debugLineNum = 164;BA.debugLine="cvs.DrawLine(x, testGrid.top, x, testG";
_v5.DrawLine(_x,_vvvvvv5.top,_x,_vvvvvv5.btm,_vvvvvv3.Colors.DarkGray,1);
 }
};
 //BA.debugLineNum = 166;BA.debugLine="For y = testGrid.top To (testGrid.top + te";
{
final int step4 = (int) (_vvvvvv5.gridStep);
final int limit4 = (int) ((_vvvvvv5.top+_vvvvvv5.height));
_y = (int) (_vvvvvv5.top) ;
for (;(step4 > 0 && _y <= limit4) || (step4 < 0 && _y >= limit4) ;_y = ((int)(0 + _y + step4))  ) {
 //BA.debugLineNum = 167;BA.debugLine="cvs.DrawLine (testGrid.left, y, testGr";
_v5.DrawLine(_vvvvvv5.left,_y,_vvvvvv5.right,_y,_vvvvvv3.Colors.DarkGray,1);
 }
};
 //BA.debugLineNum = 169;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4j.objects.JFX.PaintWrapper  _vvvv0(int _gridvalue) throws Exception{
 //BA.debugLineNum = 171;BA.debugLine="Sub GetColor(gridValue As Int) As Paint";
 //BA.debugLineNum = 172;BA.debugLine="Return colorMap.Get(gridValue)";
if (true) return (anywheresoftware.b4j.objects.JFX.PaintWrapper) anywheresoftware.b4a.AbsObjectWrapper.ConvertToWrapper(new anywheresoftware.b4j.objects.JFX.PaintWrapper(), (javafx.scene.paint.Paint)(_vvvvvv2.Get((Object)(_gridvalue))));
 //BA.debugLineNum = 173;BA.debugLine="End Sub";
return null;
}
public static b4j.example.main._point  _vvvvv1(int _gridx,int _gridy) throws Exception{
b4j.example.main._point _p = null;
 //BA.debugLineNum = 89;BA.debugLine="Sub GridToPixel(gridX As Int, gridY As Int) As";
 //BA.debugLineNum = 90;BA.debugLine="Dim p As Point : p.initialize";
_p = new b4j.example.main._point();
 //BA.debugLineNum = 90;BA.debugLine="Dim p As Point : p.initialize";
_p.Initialize();
 //BA.debugLineNum = 91;BA.debugLine="p.x = (testGrid.left + (gridX * testGrid.g";
_p.x = (_vvvvvv5.left+(_gridx*_vvvvvv5.gridStep))+(_vvvvvv5.gridStep/(double)2);
 //BA.debugLineNum = 92;BA.debugLine="p.y = (testGrid.top  + (gridY * testGrid.g";
_p.y = (_vvvvvv5.top+(_gridy*_vvvvvv5.gridStep))+(_vvvvvv5.gridStep/(double)2);
 //BA.debugLineNum = 93;BA.debugLine="Return p";
if (true) return _p;
 //BA.debugLineNum = 94;BA.debugLine="End Sub";
return null;
}
public static String  _vvvvv2() throws Exception{
 //BA.debugLineNum = 65;BA.debugLine="Sub InitCode";
 //BA.debugLineNum = 66;BA.debugLine="oX = cvs.Width / 2";
_vv3 = _v5.getWidth()/(double)2;
 //BA.debugLineNum = 67;BA.debugLine="oY = cvs.Height / 2";
_vv4 = _v5.getHeight()/(double)2;
 //BA.debugLineNum = 69;BA.debugLine="InitializeGrid(30, 30, 20)";
_vvvvv3((int) (30),(int) (30),20);
 //BA.debugLineNum = 70;BA.debugLine="RayCaster.Initialize(cvs)";
_vvvvvvv1._initialize(_v5);
 //BA.debugLineNum = 74;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvv3(int _sizex,int _sizey,double _gridstep) throws Exception{
int[][] _matrix = null;
 //BA.debugLineNum = 107;BA.debugLine="Sub InitializeGrid(sizeX As Int, sizeY As Int,";
 //BA.debugLineNum = 108;BA.debugLine="If gridStep <= 0 Then gridStep = 10";
if (_gridstep<=0) { 
_gridstep = 10;};
 //BA.debugLineNum = 109;BA.debugLine="If sizeX <= 0 Or sizeY <= 0 Then";
if (_sizex<=0 || _sizey<=0) { 
 //BA.debugLineNum = 110;BA.debugLine="sizeX = cvs.Width  / gridStep";
_sizex = (int) (_v5.getWidth()/(double)_gridstep);
 //BA.debugLineNum = 111;BA.debugLine="sizeY = cvs.Height / gridStep";
_sizey = (int) (_v5.getHeight()/(double)_gridstep);
 };
 //BA.debugLineNum = 113;BA.debugLine="testGrid.Initialize";
_vvvvvv5.Initialize();
 //BA.debugLineNum = 114;BA.debugLine="testGrid.gridStep = gridStep";
_vvvvvv5.gridStep = _gridstep;
 //BA.debugLineNum = 115;BA.debugLine="testGrid.sizeX    = sizeX";
_vvvvvv5.sizeX = _sizex;
 //BA.debugLineNum = 116;BA.debugLine="testGrid.sizeY    = sizeY";
_vvvvvv5.sizeY = _sizey;
 //BA.debugLineNum = 117;BA.debugLine="testGrid.width    = sizeX * gridStep";
_vvvvvv5.width = _sizex*_gridstep;
 //BA.debugLineNum = 118;BA.debugLine="testGrid.height   = sizeY * gridStep";
_vvvvvv5.height = _sizey*_gridstep;
 //BA.debugLineNum = 119;BA.debugLine="testGrid.left     = cvsCenterX - (testGrid";
_vvvvvv5.left = _vvvvv0-(_vvvvvv5.width/(double)2);
 //BA.debugLineNum = 120;BA.debugLine="testGrid.top      = cvsCenterY - (testGrid";
_vvvvvv5.top = _vvvvvv1-(_vvvvvv5.height/(double)2);
 //BA.debugLineNum = 121;BA.debugLine="testGrid.right    = testGrid.left + testGr";
_vvvvvv5.right = _vvvvvv5.left+_vvvvvv5.width;
 //BA.debugLineNum = 122;BA.debugLine="testGrid.btm      = testGrid.top  + testGr";
_vvvvvv5.btm = _vvvvvv5.top+_vvvvvv5.height;
 //BA.debugLineNum = 123;BA.debugLine="Dim matrix(sizeX, sizeY) As Int : testGrid";
_matrix = new int[_sizex][];
{
int d0 = _matrix.length;
int d1 = _sizey;
for (int i0 = 0;i0 < d0;i0++) {
_matrix[i0] = new int[d1];
}
}
;
 //BA.debugLineNum = 123;BA.debugLine="Dim matrix(sizeX, sizeY) As Int : testGrid";
_vvvvvv5.matrix = _matrix;
 //BA.debugLineNum = 124;BA.debugLine="End Sub";
return "";
}
public static String  _mainloop_tick() throws Exception{
 //BA.debugLineNum = 76;BA.debugLine="Sub MainLoop_Tick";
 //BA.debugLineNum = 77;BA.debugLine="DrawAll";
_vvvv5();
 //BA.debugLineNum = 78;BA.debugLine="End Sub";
return "";
}
public static b4j.example.main._point  _vvvvv4(double _x,double _y) throws Exception{
b4j.example.main._point _p = null;
 //BA.debugLineNum = 97;BA.debugLine="Sub PixelToGrid(x As Double, y As Double) As P";
 //BA.debugLineNum = 98;BA.debugLine="Dim p As Point : p.initialize";
_p = new b4j.example.main._point();
 //BA.debugLineNum = 98;BA.debugLine="Dim p As Point : p.initialize";
_p.Initialize();
 //BA.debugLineNum = 99;BA.debugLine="p.x = Floor(Max(Min((-testGrid.left + x) /";
_p.x = anywheresoftware.b4a.keywords.Common.Floor(anywheresoftware.b4a.keywords.Common.Max(anywheresoftware.b4a.keywords.Common.Min((-_vvvvvv5.left+_x)/(double)_vvvvvv5.gridStep,_vvvvvv5.sizeX-1),0));
 //BA.debugLineNum = 100;BA.debugLine="p.y = Floor(Max(Min((-testGrid.top  + y) /";
_p.y = anywheresoftware.b4a.keywords.Common.Floor(anywheresoftware.b4a.keywords.Common.Max(anywheresoftware.b4a.keywords.Common.Min((-_vvvvvv5.top+_y)/(double)_vvvvvv5.gridStep,_vvvvvv5.sizeY-1),0));
 //BA.debugLineNum = 101;BA.debugLine="Return p";
if (true) return _p;
 //BA.debugLineNum = 102;BA.debugLine="End Sub";
return null;
}

private static boolean processGlobalsRun;
public static void initializeProcessGlobals() {
    
    if (main.processGlobalsRun == false) {
	    main.processGlobalsRun = true;
		try {
		        main._process_globals();
raycaster._process_globals();
		
        } catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
}public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 15;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 16;BA.debugLine="Type Grid(gridStep As Double, sizeX As Int";
;
 //BA.debugLineNum = 17;BA.debugLine="Type Point(x As Double, y As Double, a As";
;
 //BA.debugLineNum = 19;BA.debugLine="Private fx As JFX";
_vvvvvv3 = new anywheresoftware.b4j.objects.JFX();
 //BA.debugLineNum = 20;BA.debugLine="Private MainForm As Form";
_vvvvv7 = new anywheresoftware.b4j.objects.Form();
 //BA.debugLineNum = 21;BA.debugLine="Private cvs As Canvas";
_v5 = new anywheresoftware.b4j.objects.CanvasWrapper();
 //BA.debugLineNum = 22;BA.debugLine="Private cvsCenterX As Double";
_vvvvv0 = 0;
 //BA.debugLineNum = 23;BA.debugLine="Private cvsCenterY As Double";
_vvvvvv1 = 0;
 //BA.debugLineNum = 25;BA.debugLine="Private testGrid As Grid";
_vvvvvv5 = new b4j.example.main._grid();
 //BA.debugLineNum = 26;BA.debugLine="Private MainLoop As Timer";
_vvvvvv4 = new anywheresoftware.b4a.objects.Timer();
 //BA.debugLineNum = 28;BA.debugLine="Private colorMap As Map";
_vvvvvv2 = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 29;BA.debugLine="Private justDoIt As Boolean";
_vvvvvv0 = false;
 //BA.debugLineNum = 31;BA.debugLine="Private oX, oY As Double";
_vv3 = 0;
_vv4 = 0;
 //BA.debugLineNum = 32;BA.debugLine="Private mX, mY As Int";
_vvvvvv6 = 0;
_vvvvvv7 = 0;
 //BA.debugLineNum = 33;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvv5(int _densitypercentage) throws Exception{
int _lowest = 0;
int _highest = 0;
Object _key = null;
int _x = 0;
int _y = 0;
 //BA.debugLineNum = 126;BA.debugLine="Sub RandomlyPopulateGrid(densityPercentage As";
 //BA.debugLineNum = 127;BA.debugLine="Dim lowest = 1, highest As Int";
_lowest = (int) (1);
_highest = 0;
 //BA.debugLineNum = 128;BA.debugLine="For Each key In colorMap.Keys : highest =";
{
final anywheresoftware.b4a.BA.IterableList group2 = _vvvvvv2.Keys();
final int groupLen2 = group2.getSize()
;int index2 = 0;
;
for (; index2 < groupLen2;index2++){
_key = group2.Get(index2);
 //BA.debugLineNum = 128;BA.debugLine="For Each key In colorMap.Keys : highest =";
_highest = (int) (anywheresoftware.b4a.keywords.Common.Max(_highest,(double)(BA.ObjectToNumber(_key))));
 }
};
 //BA.debugLineNum = 129;BA.debugLine="For x = 0 To testGrid.sizeX - 1";
{
final int step5 = 1;
final int limit5 = (int) (_vvvvvv5.sizeX-1);
_x = (int) (0) ;
for (;(step5 > 0 && _x <= limit5) || (step5 < 0 && _x >= limit5) ;_x = ((int)(0 + _x + step5))  ) {
 //BA.debugLineNum = 130;BA.debugLine="For y = 0 To testGrid.sizeY - 1";
{
final int step6 = 1;
final int limit6 = (int) (_vvvvvv5.sizeY-1);
_y = (int) (0) ;
for (;(step6 > 0 && _y <= limit6) || (step6 < 0 && _y >= limit6) ;_y = ((int)(0 + _y + step6))  ) {
 //BA.debugLineNum = 131;BA.debugLine="If Rnd(0, 101) > densityPercentage";
if (anywheresoftware.b4a.keywords.Common.Rnd((int) (0),(int) (101))>_densitypercentage) { 
if (true) continue;};
 //BA.debugLineNum = 132;BA.debugLine="testGrid.matrix(x, y) = 1'Rnd(lowe";
_vvvvvv5.matrix[_x][_y] = (int) (1);
 }
};
 }
};
 //BA.debugLineNum = 135;BA.debugLine="End Sub";
return "";
}
public static String  _vvvvv6() throws Exception{
 //BA.debugLineNum = 60;BA.debugLine="Sub StopDoingIt";
 //BA.debugLineNum = 61;BA.debugLine="justDoIt = False";
_vvvvvv0 = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 62;BA.debugLine="End Sub";
return "";
}
}
