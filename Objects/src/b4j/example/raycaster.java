package b4j.example;


import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.debug.*;

public class raycaster extends Object{
public static raycaster mostCurrent = new raycaster();

public static BA ba;
static {
		ba = new  anywheresoftware.b4j.objects.FxBA("b4j.example", "b4j.example.raycaster", null);
		ba.loadHtSubs(raycaster.class);
        if (ba.getClass().getName().endsWith("ShellBA")) {
			
			ba.raiseEvent2(null, true, "SHELL", false);
			ba.raiseEvent2(null, true, "CREATE", true, "b4j.example.raycaster", ba);
		}
	}
    public static Class<?> getObject() {
		return raycaster.class;
	}

 public static anywheresoftware.b4a.keywords.Common __c = null;
public static int _x_coord = 0;
public static int _y_coord = 0;
public static int _o_angle = 0;
public static double _half_pi = 0;
public static anywheresoftware.b4j.objects.CanvasWrapper _v5 = null;
public static anywheresoftware.b4a.objects.collections.List _v6 = null;
public static anywheresoftware.b4a.objects.collections.List _v7 = null;
public static anywheresoftware.b4a.objects.collections.List _v0 = null;
public static anywheresoftware.b4a.objects.collections.Map _vv1 = null;
public static anywheresoftware.b4a.objects.collections.Map _vv2 = null;
public static double _vv3 = 0;
public static double _vv4 = 0;
public static double _vv5 = 0;
public static double _vv6 = 0;
public static double _vv7 = 0;
public static double _vv0 = 0;
public static long _vvv1 = 0L;
public static b4j.example.main _vvvvvvv2 = null;
public static class _pointangle{
public boolean IsInitialized;
public double x;
public double y;
public double a;
public void Initialize() {
IsInitialized = true;
x = 0;
y = 0;
a = 0;
}
@Override
		public String toString() {
			return BA.TypeToString(this, false);
		}}
public static String  _vvv2(double _x,double _y) throws Exception{
double _pangle = 0;
String _key = "";
 //BA.debugLineNum = 373;BA.debugLine="Sub addPoint(x As Double, y As Double)";
 //BA.debugLineNum = 374;BA.debugLine="Dim pAngle = calcAngle(x, y) As Double";
_pangle = _vvv4(_x,_y);
 //BA.debugLineNum = 375;BA.debugLine="Dim key = xyKey(x, y) As String";
_key = _vvvv3(_x,_y);
 //BA.debugLineNum = 376;BA.debugLine="If isNewAngle(pAngle) Then";
if (_vvvv1(_pangle)) { 
 //BA.debugLineNum = 377;BA.debugLine="pMap.Put( _             key, Array As Doub";
_vv1.Put((Object)(_key),(Object)(new double[]{_x,_y,_pangle}));
 };
 //BA.debugLineNum = 381;BA.debugLine="angleBuffer.Add(pAngle)";
_v0.Add((Object)(_pangle));
 //BA.debugLineNum = 382;BA.debugLine="End Sub";
return "";
}
public static boolean  _vvv3(anywheresoftware.b4a.objects.collections.List _lst,double _a,double _b,double _lowest,double _highest) throws Exception{
int _i = 0;
double[] _range = null;
 //BA.debugLineNum = 447;BA.debugLine="Sub AddRange(lst As List, a As Double, b As Double";
 //BA.debugLineNum = 448;BA.debugLine="If a > highest Or b > highest Or a < lowest Or";
if (_a>_highest || _b>_highest || _a<_lowest || _b<_lowest) { 
if (true) return anywheresoftware.b4a.keywords.Common.False;};
 //BA.debugLineNum = 450;BA.debugLine="Dim i As Int";
_i = 0;
 //BA.debugLineNum = 451;BA.debugLine="If a > b Then";
if (_a>_b) { 
 //BA.debugLineNum = 452;BA.debugLine="i = lst.Size - 1";
_i = (int) (_lst.getSize()-1);
 //BA.debugLineNum = 453;BA.debugLine="Do While i >= 0";
while (_i>=0) {
 //BA.debugLineNum = 454;BA.debugLine="Dim range() = lst.Get(i) As Double";
_range = (double[])(_lst.Get(_i));
 //BA.debugLineNum = 455;BA.debugLine="If (a >= range(0) And a <= range(1)) O";
if ((_a>=_range[(int) (0)] && _a<=_range[(int) (1)]) || (_range[(int) (0)]>=_a && _range[(int) (0)]<=_highest)) { 
 //BA.debugLineNum = 456;BA.debugLine="a = Min(a, range(0))";
_a = anywheresoftware.b4a.keywords.Common.Min(_a,_range[(int) (0)]);
 //BA.debugLineNum = 457;BA.debugLine="lst.RemoveAt(i)";
_lst.RemoveAt(_i);
 };
 //BA.debugLineNum = 459;BA.debugLine="i = i - 1";
_i = (int) (_i-1);
 }
;
 //BA.debugLineNum = 461;BA.debugLine="lst.Add(Array As Double(a, highest))";
_lst.Add((Object)(new double[]{_a,_highest}));
 //BA.debugLineNum = 463;BA.debugLine="i = lst.Size - 1";
_i = (int) (_lst.getSize()-1);
 //BA.debugLineNum = 464;BA.debugLine="Do While i >= 0";
while (_i>=0) {
 //BA.debugLineNum = 465;BA.debugLine="Dim range() = lst.Get(i) As Double";
_range = (double[])(_lst.Get(_i));
 //BA.debugLineNum = 466;BA.debugLine="If (b >= range(0) And b <= range(1)) O";
if ((_b>=_range[(int) (0)] && _b<=_range[(int) (1)]) || (_range[(int) (1)]>=_lowest && _range[(int) (1)]<=_b)) { 
 //BA.debugLineNum = 467;BA.debugLine="b = Max(b, range(1))";
_b = anywheresoftware.b4a.keywords.Common.Max(_b,_range[(int) (1)]);
 //BA.debugLineNum = 468;BA.debugLine="lst.RemoveAt(i)";
_lst.RemoveAt(_i);
 };
 //BA.debugLineNum = 470;BA.debugLine="i = i - 1";
_i = (int) (_i-1);
 }
;
 //BA.debugLineNum = 472;BA.debugLine="lst.Add(Array As Double(lowest, b))";
_lst.Add((Object)(new double[]{_lowest,_b}));
 }else {
 //BA.debugLineNum = 475;BA.debugLine="i = lst.Size - 1";
_i = (int) (_lst.getSize()-1);
 //BA.debugLineNum = 476;BA.debugLine="Do While i >= 0";
while (_i>=0) {
 //BA.debugLineNum = 477;BA.debugLine="Dim range() = lst.Get(i) As Double";
_range = (double[])(_lst.Get(_i));
 //BA.debugLineNum = 478;BA.debugLine="If (a >= range(0) And a <= range(1)) O";
if ((_a>=_range[(int) (0)] && _a<=_range[(int) (1)]) || (_b>=_range[(int) (0)] && _b<=_range[(int) (1)]) || (_range[(int) (0)]>=_a && _range[(int) (0)]<=_b) || (_range[(int) (1)]>=_a && _range[(int) (1)]<=_b)) { 
 //BA.debugLineNum = 480;BA.debugLine="a = Min(a, range(0))";
_a = anywheresoftware.b4a.keywords.Common.Min(_a,_range[(int) (0)]);
 //BA.debugLineNum = 481;BA.debugLine="b = Max(b, range(1))";
_b = anywheresoftware.b4a.keywords.Common.Max(_b,_range[(int) (1)]);
 //BA.debugLineNum = 482;BA.debugLine="lst.RemoveAt(i)";
_lst.RemoveAt(_i);
 };
 //BA.debugLineNum = 484;BA.debugLine="i = i - 1";
_i = (int) (_i-1);
 }
;
 //BA.debugLineNum = 486;BA.debugLine="lst.Add(Array As Double(a, b))";
_lst.Add((Object)(new double[]{_a,_b}));
 };
 //BA.debugLineNum = 488;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 489;BA.debugLine="End Sub";
return false;
}
public static double  _vvv4(double _x,double _y) throws Exception{
 //BA.debugLineNum = 365;BA.debugLine="Sub calcAngle(x As Double, y As Double) As Double";
 //BA.debugLineNum = 366;BA.debugLine="Return ATan2(y - oY, x - oX)";
if (true) return anywheresoftware.b4a.keywords.Common.ATan2(_y-_vv4,_x-_vv3);
 //BA.debugLineNum = 367;BA.debugLine="End Sub";
return 0;
}
public static String  _vvv5(int[][] _matrix,int _matrixsizex,int _matrixsizey,double _gridstep,double _left,double _top,double _originx,double _originy,int _mx,int _my) throws Exception{
anywheresoftware.b4j.objects.JFX _fx = null;
int _iter = 0;
int _count = 0;
int _moves = 0;
short[] _xdir = null;
short[] _ydir = null;
short _angle = (short)0;
boolean _exitflag = false;
double _north = 0;
double _south = 0;
double _east = 0;
double _west = 0;
double[] _singleentry = null;
Object _key = null;
int _xx = 0;
int _yy = 0;
double _x = 0;
double _y = 0;
double _a = 0;
double _cosa = 0;
double _sina = 0;
double _cosp = 0;
double _sinp = 0;
double _rangedemultiplier = 0;
double _thisangle = 0;
int _raysize = 0;
double[] _xya = null;
double[] _poi = null;
double _perp = 0;
double _xxp1 = 0;
double _yyp1 = 0;
double _xxp2 = 0;
double _yyp2 = 0;
double _rx = 0;
double _ry = 0;
double _dst = 0;
double _new_x = 0;
double _new_y = 0;
anywheresoftware.b4a.objects.collections.List _poilist = null;
anywheresoftware.b4a.objects.collections.Map _blocklist = null;
int _signmultiplier = 0;
double[] _newblk = null;
int _nbx = 0;
int _nby = 0;
 //BA.debugLineNum = 37;BA.debugLine="Sub Cast(matrix(,) As Int, matrixSizeX As Int, mat";
 //BA.debugLineNum = 38;BA.debugLine="Dim fx As JFX";
_fx = new anywheresoftware.b4j.objects.JFX();
 //BA.debugLineNum = 40;BA.debugLine="ts = DateTime.now";
_vvv1 = anywheresoftware.b4a.keywords.Common.DateTime.getNow();
 //BA.debugLineNum = 42;BA.debugLine="oX = originX - left";
_vv3 = _originx-_left;
 //BA.debugLineNum = 43;BA.debugLine="oY = originY - top";
_vv4 = _originy-_top;
 //BA.debugLineNum = 45;BA.debugLine="lstAngles.Clear";
_v6.Clear();
 //BA.debugLineNum = 46;BA.debugLine="lstPoints.Clear";
_v7.Clear();
 //BA.debugLineNum = 47;BA.debugLine="angleBuffer.Clear";
_v0.Clear();
 //BA.debugLineNum = 49;BA.debugLine="pMap.Clear";
_vv1.Clear();
 //BA.debugLineNum = 50;BA.debugLine="pFck.Clear";
_vv2.Clear();
 //BA.debugLineNum = 52;BA.debugLine="Dim iter   = 0";
_iter = (int) (0);
 //BA.debugLineNum = 53;BA.debugLine="Dim count  = matrixSizeX * matrixSizeY";
_count = (int) (_matrixsizex*_matrixsizey);
 //BA.debugLineNum = 54;BA.debugLine="Dim moves  = 1";
_moves = (int) (1);
 //BA.debugLineNum = 55;BA.debugLine="Dim xdir() = Array As Short( 0,  1,  0, -1)";
_xdir = new short[]{(short) (0),(short) (1),(short) (0),(short) (-1)};
 //BA.debugLineNum = 56;BA.debugLine="Dim ydir() = Array As Short(-1,  0,  1,  0)";
_ydir = new short[]{(short) (-1),(short) (0),(short) (1),(short) (0)};
 //BA.debugLineNum = 57;BA.debugLine="Dim angle  = 0";
_angle = (short) (0);
 //BA.debugLineNum = 58;BA.debugLine="Dim mX     = Floor(Max(Min(oX / gridStep, matr";
_mx = (int) (anywheresoftware.b4a.keywords.Common.Floor(anywheresoftware.b4a.keywords.Common.Max(anywheresoftware.b4a.keywords.Common.Min(_vv3/(double)_gridstep,_matrixsizex-1),0)));
 //BA.debugLineNum = 59;BA.debugLine="Dim mY     = Floor(Max(Min(oY / gridStep, matr";
_my = (int) (anywheresoftware.b4a.keywords.Common.Floor(anywheresoftware.b4a.keywords.Common.Max(anywheresoftware.b4a.keywords.Common.Min(_vv4/(double)_gridstep,_matrixsizey-1),0)));
 //BA.debugLineNum = 61;BA.debugLine="Dim exitFlag = False As Boolean";
_exitflag = anywheresoftware.b4a.keywords.Common.False;
 //BA.debugLineNum = 62;BA.debugLine="Dim north, south, east, west As Double";
_north = 0;
_south = 0;
_east = 0;
_west = 0;
 //BA.debugLineNum = 64;BA.debugLine="Do While count > 0";
while (_count>0) {
 //BA.debugLineNum = 65;BA.debugLine="iter = moves";
_iter = _moves;
 //BA.debugLineNum = 66;BA.debugLine="Do While iter > 0";
while (_iter>0) {
 //BA.debugLineNum = 67;BA.debugLine="If lstAngles.Size == 1 Then";
if (_v6.getSize()==1) { 
 //BA.debugLineNum = 68;BA.debugLine="Dim singleEntry() = lstAngles.Get(";
_singleentry = (double[])(_v6.Get((int) (0)));
 //BA.debugLineNum = 69;BA.debugLine="If singleEntry(0) == -cPI And sing";
if (_singleentry[(int) (0)]==-anywheresoftware.b4a.keywords.Common.cPI && _singleentry[(int) (1)]==anywheresoftware.b4a.keywords.Common.cPI) { 
 //BA.debugLineNum = 70;BA.debugLine="exitFlag = True";
_exitflag = anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 71;BA.debugLine="Exit";
if (true) break;
 };
 };
 //BA.debugLineNum = 76;BA.debugLine="iter = iter - 1";
_iter = (int) (_iter-1);
 //BA.debugLineNum = 77;BA.debugLine="mX = mX + xdir(angle)";
_mx = (int) (_mx+_xdir[(int) (_angle)]);
 //BA.debugLineNum = 78;BA.debugLine="mY = mY + ydir(angle)";
_my = (int) (_my+_ydir[(int) (_angle)]);
 //BA.debugLineNum = 79;BA.debugLine="If mX < 0 Or mX >= matrixSizeX Or mY <";
if (_mx<0 || _mx>=_matrixsizex || _my<0 || _my>=_matrixsizey || _matrix[_mx][_my]<1) { 
if (true) continue;};
 //BA.debugLineNum = 82;BA.debugLine="west  = mX    * gridStep";
_west = _mx*_gridstep;
 //BA.debugLineNum = 83;BA.debugLine="east  = west  + gridStep";
_east = _west+_gridstep;
 //BA.debugLineNum = 84;BA.debugLine="north = mY    * gridStep";
_north = _my*_gridstep;
 //BA.debugLineNum = 85;BA.debugLine="south = north + gridStep";
_south = _north+_gridstep;
 //BA.debugLineNum = 87;BA.debugLine="If oX >= west And oX <= east Then";
if (_vv3>=_west && _vv3<=_east) { 
 //BA.debugLineNum = 88;BA.debugLine="If oY < north Then";
if (_vv4<_north) { 
 //BA.debugLineNum = 89;BA.debugLine="remPoint(east, south)";
_vvvv2(_east,_south);
 //BA.debugLineNum = 90;BA.debugLine="remPoint(west, south)";
_vvvv2(_west,_south);
 //BA.debugLineNum = 91;BA.debugLine="addPoint(east, north)";
_vvv2(_east,_north);
 //BA.debugLineNum = 92;BA.debugLine="addPoint(west, north)";
_vvv2(_west,_north);
 //BA.debugLineNum = 93;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 }else if(_vv4>_south) { 
 //BA.debugLineNum = 95;BA.debugLine="remPoint(west, north)";
_vvvv2(_west,_north);
 //BA.debugLineNum = 96;BA.debugLine="remPoint(east, north)";
_vvvv2(_east,_north);
 //BA.debugLineNum = 97;BA.debugLine="addPoint(west, south)";
_vvv2(_west,_south);
 //BA.debugLineNum = 98;BA.debugLine="addPoint(east, south)";
_vvv2(_east,_south);
 //BA.debugLineNum = 99;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 };
 }else if(_vv4>=_north && _vv4<=_south) { 
 //BA.debugLineNum = 102;BA.debugLine="If oX > east Then";
if (_vv3>_east) { 
 //BA.debugLineNum = 103;BA.debugLine="remPoint(west, south)";
_vvvv2(_west,_south);
 //BA.debugLineNum = 104;BA.debugLine="remPoint(west, north)";
_vvvv2(_west,_north);
 //BA.debugLineNum = 105;BA.debugLine="addPoint(east, south)";
_vvv2(_east,_south);
 //BA.debugLineNum = 106;BA.debugLine="addPoint(east, north)";
_vvv2(_east,_north);
 //BA.debugLineNum = 107;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 }else if(_vv3<_west) { 
 //BA.debugLineNum = 109;BA.debugLine="remPoint(east, north)";
_vvvv2(_east,_north);
 //BA.debugLineNum = 110;BA.debugLine="remPoint(east, south)";
_vvvv2(_east,_south);
 //BA.debugLineNum = 111;BA.debugLine="addPoint(west, north)";
_vvv2(_west,_north);
 //BA.debugLineNum = 112;BA.debugLine="addPoint(west, south)";
_vvv2(_west,_south);
 //BA.debugLineNum = 113;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 };
 }else if(_vv3<_west) { 
 //BA.debugLineNum = 116;BA.debugLine="If oY < north Then";
if (_vv4<_north) { 
 //BA.debugLineNum = 117;BA.debugLine="remPoint(east, south)";
_vvvv2(_east,_south);
 //BA.debugLineNum = 118;BA.debugLine="addPoint(east, north)";
_vvv2(_east,_north);
 //BA.debugLineNum = 119;BA.debugLine="addPoint(west, north)";
_vvv2(_west,_north);
 //BA.debugLineNum = 120;BA.debugLine="addPoint(west, south)";
_vvv2(_west,_south);
 //BA.debugLineNum = 121;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),(double)(BA.ObjectToNumber(_v0.Get((int) (2)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 //BA.debugLineNum = 122;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 }else {
 //BA.debugLineNum = 124;BA.debugLine="remPoint(east, north)";
_vvvv2(_east,_north);
 //BA.debugLineNum = 125;BA.debugLine="addPoint(west, north)";
_vvv2(_west,_north);
 //BA.debugLineNum = 126;BA.debugLine="addPoint(west, south)";
_vvv2(_west,_south);
 //BA.debugLineNum = 127;BA.debugLine="addPoint(east, south)";
_vvv2(_east,_south);
 //BA.debugLineNum = 128;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),(double)(BA.ObjectToNumber(_v0.Get((int) (2)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 //BA.debugLineNum = 129;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 };
 }else if(_vv3>_east) { 
 //BA.debugLineNum = 132;BA.debugLine="If oY < north Then";
if (_vv4<_north) { 
 //BA.debugLineNum = 133;BA.debugLine="remPoint(west, south)";
_vvvv2(_west,_south);
 //BA.debugLineNum = 134;BA.debugLine="addPoint(east, south)";
_vvv2(_east,_south);
 //BA.debugLineNum = 135;BA.debugLine="addPoint(east, north)";
_vvv2(_east,_north);
 //BA.debugLineNum = 136;BA.debugLine="addPoint(west, north)";
_vvv2(_west,_north);
 //BA.debugLineNum = 137;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),(double)(BA.ObjectToNumber(_v0.Get((int) (2)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 //BA.debugLineNum = 138;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 }else {
 //BA.debugLineNum = 140;BA.debugLine="remPoint(west, north)";
_vvvv2(_west,_north);
 //BA.debugLineNum = 141;BA.debugLine="addPoint(west, south)";
_vvv2(_west,_south);
 //BA.debugLineNum = 142;BA.debugLine="addPoint(east, south)";
_vvv2(_east,_south);
 //BA.debugLineNum = 143;BA.debugLine="addPoint(east, north)";
_vvv2(_east,_north);
 //BA.debugLineNum = 144;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),(double)(BA.ObjectToNumber(_v0.Get((int) (2)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 //BA.debugLineNum = 145;BA.debugLine="AddRange(lstAngles, angleBuffe";
_vvv3(_v6,(double)(BA.ObjectToNumber(_v0.Get((int) (0)))),(double)(BA.ObjectToNumber(_v0.Get((int) (1)))),-anywheresoftware.b4a.keywords.Common.cPI,anywheresoftware.b4a.keywords.Common.cPI);
 };
 };
 //BA.debugLineNum = 148;BA.debugLine="angleBuffer.Clear";
_v0.Clear();
 }
;
 //BA.debugLineNum = 150;BA.debugLine="If exitFlag Then Exit";
if (_exitflag) { 
if (true) break;};
 //BA.debugLineNum = 152;BA.debugLine="angle = (angle + 1) Mod 4";
_angle = (short) ((_angle+1)%4);
 //BA.debugLineNum = 153;BA.debugLine="moves = moves + Floor(1 - angle Mod 2)";
_moves = (int) (_moves+anywheresoftware.b4a.keywords.Common.Floor(1-_angle%2));
 //BA.debugLineNum = 154;BA.debugLine="count = count - 1";
_count = (int) (_count-1);
 }
;
 //BA.debugLineNum = 157;BA.debugLine="gMinX = 0";
_vv5 = 0;
 //BA.debugLineNum = 158;BA.debugLine="gMinY = 0";
_vv6 = 0;
 //BA.debugLineNum = 159;BA.debugLine="gMaxX = matrixSizeX * gridStep";
_vv7 = _matrixsizex*_gridstep;
 //BA.debugLineNum = 160;BA.debugLine="gMaxY = matrixSizeY * gridStep";
_vv0 = _matrixsizey*_gridstep;
 //BA.debugLineNum = 162;BA.debugLine="addPoint(gMinX, gMaxY)";
_vvv2(_vv5,_vv0);
 //BA.debugLineNum = 163;BA.debugLine="addPoint(gMinX, gMinY)";
_vvv2(_vv5,_vv6);
 //BA.debugLineNum = 164;BA.debugLine="addPoint(gMaxX, gMinY)";
_vvv2(_vv7,_vv6);
 //BA.debugLineNum = 165;BA.debugLine="addPoint(gMaxX, gMaxY)";
_vvv2(_vv7,_vv0);
 //BA.debugLineNum = 168;BA.debugLine="For Each key In pFck.Keys";
{
final anywheresoftware.b4a.BA.IterableList group114 = _vv2.Keys();
final int groupLen114 = group114.getSize()
;int index114 = 0;
;
for (; index114 < groupLen114;index114++){
_key = group114.Get(index114);
 //BA.debugLineNum = 169;BA.debugLine="pMap.Remove(key)";
_vv1.Remove(_key);
 }
};
 //BA.debugLineNum = 177;BA.debugLine="Dim xx, yy As Int";
_xx = 0;
_yy = 0;
 //BA.debugLineNum = 178;BA.debugLine="Dim x, y, a As Double";
_x = 0;
_y = 0;
_a = 0;
 //BA.debugLineNum = 179;BA.debugLine="Dim cosA, sinA, cosP, sinP As Double";
_cosa = 0;
_sina = 0;
_cosp = 0;
_sinp = 0;
 //BA.debugLineNum = 180;BA.debugLine="Dim rangeDemultiplier = 0.000001, thisAngle As";
_rangedemultiplier = 0.000001;
_thisangle = 0;
 //BA.debugLineNum = 181;BA.debugLine="Dim raySize = gridStep * (matrixSizeX + matrix";
_raysize = (int) (_gridstep*(_matrixsizex+_matrixsizey));
 //BA.debugLineNum = 184;BA.debugLine="Dim xya()     As Double";
_xya = new double[(int) (0)];
;
 //BA.debugLineNum = 185;BA.debugLine="Dim poi()     As Double";
_poi = new double[(int) (0)];
;
 //BA.debugLineNum = 186;BA.debugLine="Dim perp      As Double";
_perp = 0;
 //BA.debugLineNum = 187;BA.debugLine="Dim cosP      As Double";
_cosp = 0;
 //BA.debugLineNum = 188;BA.debugLine="Dim sinP      As Double";
_sinp = 0;
 //BA.debugLineNum = 189;BA.debugLine="Dim xxP1      As Double";
_xxp1 = 0;
 //BA.debugLineNum = 190;BA.debugLine="Dim yyP1      As Double";
_yyp1 = 0;
 //BA.debugLineNum = 191;BA.debugLine="Dim xxP2      As Double";
_xxp2 = 0;
 //BA.debugLineNum = 192;BA.debugLine="Dim yyP2      As Double";
_yyp2 = 0;
 //BA.debugLineNum = 193;BA.debugLine="Dim rX        As Double";
_rx = 0;
 //BA.debugLineNum = 194;BA.debugLine="Dim rY        As Double";
_ry = 0;
 //BA.debugLineNum = 195;BA.debugLine="Dim dst       As Double";
_dst = 0;
 //BA.debugLineNum = 196;BA.debugLine="Dim new_x     As Double";
_new_x = 0;
 //BA.debugLineNum = 197;BA.debugLine="Dim new_y     As Double";
_new_y = 0;
 //BA.debugLineNum = 198;BA.debugLine="Dim poiList   As List";
_poilist = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 199;BA.debugLine="Dim blockList As Map";
_blocklist = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 202;BA.debugLine="poiList.Initialize";
_poilist.Initialize();
 //BA.debugLineNum = 203;BA.debugLine="For Each key In pMap.Keys";
{
final anywheresoftware.b4a.BA.IterableList group139 = _vv1.Keys();
final int groupLen139 = group139.getSize()
;int index139 = 0;
;
for (; index139 < groupLen139;index139++){
_key = group139.Get(index139);
 //BA.debugLineNum = 204;BA.debugLine="xya = pMap.Get(key)";
_xya = (double[])(_vv1.Get(_key));
 //BA.debugLineNum = 205;BA.debugLine="x   = xya(X_COORD)";
_x = _xya[_x_coord];
 //BA.debugLineNum = 206;BA.debugLine="y   = xya(Y_COORD)";
_y = _xya[_y_coord];
 //BA.debugLineNum = 207;BA.debugLine="a   = xya(O_ANGLE)";
_a = _xya[_o_angle];
 //BA.debugLineNum = 209;BA.debugLine="For signMultiplier = -1 To 1";
{
final int step144 = 1;
final int limit144 = (int) (1);
_signmultiplier = (int) (-1) ;
for (;(step144 > 0 && _signmultiplier <= limit144) || (step144 < 0 && _signmultiplier >= limit144) ;_signmultiplier = ((int)(0 + _signmultiplier + step144))  ) {
 //BA.debugLineNum = 210;BA.debugLine="thisAngle = a + (signMultiplier * rang";
_thisangle = _a+(_signmultiplier*_rangedemultiplier);
 //BA.debugLineNum = 211;BA.debugLine="cosA = Cos(thisAngle) : xx = (x + cosA";
_cosa = anywheresoftware.b4a.keywords.Common.Cos(_thisangle);
 //BA.debugLineNum = 211;BA.debugLine="cosA = Cos(thisAngle) : xx = (x + cosA";
_xx = (int) ((_x+_cosa)/(double)_gridstep);
 //BA.debugLineNum = 212;BA.debugLine="sinA = Sin(thisAngle) : yy = (y + sinA";
_sina = anywheresoftware.b4a.keywords.Common.Sin(_thisangle);
 //BA.debugLineNum = 212;BA.debugLine="sinA = Sin(thisAngle) : yy = (y + sinA";
_yy = (int) ((_y+_sina)/(double)_gridstep);
 //BA.debugLineNum = 213;BA.debugLine="If xx < 0 Or xx >= matrixSizeX Or yy <";
if (_xx<0 || _xx>=_matrixsizex || _yy<0 || _yy>=_matrixsizey || _matrix[_xx][_yy]==0) { 
 //BA.debugLineNum = 214;BA.debugLine="perp = a + HALF_PI";
_perp = _a+_half_pi;
 //BA.debugLineNum = 215;BA.debugLine="cosP = Cos(perp)";
_cosp = anywheresoftware.b4a.keywords.Common.Cos(_perp);
 //BA.debugLineNum = 216;BA.debugLine="sinP = Sin(perp)";
_sinp = anywheresoftware.b4a.keywords.Common.Sin(_perp);
 //BA.debugLineNum = 217;BA.debugLine="xxP1 = (x + cosP) / gridStep";
_xxp1 = (_x+_cosp)/(double)_gridstep;
 //BA.debugLineNum = 218;BA.debugLine="yyP1 = (y + sinP) / gridStep";
_yyp1 = (_y+_sinp)/(double)_gridstep;
 //BA.debugLineNum = 219;BA.debugLine="xxP2 = (x - cosP) / gridStep";
_xxp2 = (_x-_cosp)/(double)_gridstep;
 //BA.debugLineNum = 220;BA.debugLine="yyP2 = (y - sinP) / gridStep";
_yyp2 = (_y-_sinp)/(double)_gridstep;
 //BA.debugLineNum = 221;BA.debugLine="If False == ( _";
if (anywheresoftware.b4a.keywords.Common.False==((_xxp1>=0 && _xxp1<_matrixsizex && _yyp1>=0 && _yyp1<_matrixsizey && _matrix[(int) (_xxp1)][(int) (_yyp1)]>0) && (_xxp2>=0 && _xxp2<_matrixsizex && _yyp2>=0 && _yyp2<_matrixsizey && _matrix[(int) (_xxp2)][(int) (_yyp2)]>0))) { 
 //BA.debugLineNum = 225;BA.debugLine="rX = x + cosA * raySize";
_rx = _x+_cosa*_raysize;
 //BA.debugLineNum = 226;BA.debugLine="rY = y + sinA * raySize";
_ry = _y+_sina*_raysize;
 //BA.debugLineNum = 227;BA.debugLine="blockList = getBlocks(x, y, rX";
_blocklist = _vvv6(_x,_y,_rx,_ry,_gridstep,_matrixsizex,_matrixsizey);
 //BA.debugLineNum = 229;BA.debugLine="poiList.Clear";
_poilist.Clear();
 //BA.debugLineNum = 230;BA.debugLine="For Each newBlk() As Double In";
{
final anywheresoftware.b4a.BA.IterableList group163 = _blocklist.Values();
final int groupLen163 = group163.getSize()
;int index163 = 0;
;
for (; index163 < groupLen163;index163++){
_newblk = (double[])(group163.Get(index163));
 //BA.debugLineNum = 231;BA.debugLine="Dim nbX = newBlk(X_COORD)";
_nbx = (int) (_newblk[_x_coord]);
 //BA.debugLineNum = 232;BA.debugLine="Dim nbY = newBlk(Y_COORD)";
_nby = (int) (_newblk[_y_coord]);
 //BA.debugLineNum = 233;BA.debugLine="If nbX < 0 Or nbX >= matri";
if (_nbx<0 || _nbx>=_matrixsizex || _nby<0 || _nby>=_matrixsizey) { 
if (true) continue;};
 //BA.debugLineNum = 237;BA.debugLine="west  = (nbX   * gridStep)";
_west = (_nbx*_gridstep);
 //BA.debugLineNum = 238;BA.debugLine="east  = (west  + gridStep)";
_east = (_west+_gridstep);
 //BA.debugLineNum = 239;BA.debugLine="north = (nbY   * gridStep)";
_north = (_nby*_gridstep);
 //BA.debugLineNum = 240;BA.debugLine="south = (north + gridStep)";
_south = (_north+_gridstep);
 //BA.debugLineNum = 243;BA.debugLine="If nbY - 1 > 0 And matrix(";
if (_nby-1>0 && _matrix[_nbx][(int) (_nby-1)]>0) { 
 //BA.debugLineNum = 244;BA.debugLine="poi = getIntersection(";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_north,_west,_north);
 //BA.debugLineNum = 245;BA.debugLine="If poi <> Null Then po";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 //BA.debugLineNum = 247;BA.debugLine="If nbY + 1 < matrixSizeY A";
if (_nby+1<_matrixsizey && _matrix[_nbx][(int) (_nby+1)]>0) { 
 //BA.debugLineNum = 248;BA.debugLine="poi = getIntersection(";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_south,_west,_south);
 //BA.debugLineNum = 249;BA.debugLine="If poi <> Null Then po";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 //BA.debugLineNum = 251;BA.debugLine="If nbX - 1 > 0 And matrix(";
if (_nbx-1>0 && _matrix[(int) (_nbx-1)][_nby]>0) { 
 //BA.debugLineNum = 252;BA.debugLine="poi = getIntersection(";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_west,_north,_west,_south);
 //BA.debugLineNum = 253;BA.debugLine="If poi <> Null Then po";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 //BA.debugLineNum = 255;BA.debugLine="If nbX + 1 < matrixSizeX A";
if (_nbx+1<_matrixsizex && _matrix[(int) (_nbx+1)][_nby]>0) { 
 //BA.debugLineNum = 256;BA.debugLine="poi = getIntersection(";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_north,_east,_south);
 //BA.debugLineNum = 257;BA.debugLine="If poi <> Null Then po";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 //BA.debugLineNum = 260;BA.debugLine="If matrix(nbX, nbY) < 1 Th";
if (_matrix[_nbx][_nby]<1) { 
if (true) continue;};
 //BA.debugLineNum = 264;BA.debugLine="If oX >= west And oX <= east Then";
if (_vv3>=_west && _vv3<=_east) { 
 //BA.debugLineNum = 265;BA.debugLine="If oY < north Then";
if (_vv4<_north) { 
 //BA.debugLineNum = 266;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_north,_west,_north);
 //BA.debugLineNum = 267;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 }else if(_vv4>_south) { 
 //BA.debugLineNum = 269;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_south,_west,_south);
 //BA.debugLineNum = 270;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 }else if(_vv4>=_north && _vv4<=_south) { 
 //BA.debugLineNum = 273;BA.debugLine="If oX > east Then";
if (_vv3>_east) { 
 //BA.debugLineNum = 274;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_south,_east,_north);
 //BA.debugLineNum = 275;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 }else if(_vv3<_west) { 
 //BA.debugLineNum = 277;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_west,_south,_west,_north);
 //BA.debugLineNum = 278;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 }else if(_vv3<_west) { 
 //BA.debugLineNum = 281;BA.debugLine="If oY < north Then";
if (_vv4<_north) { 
 //BA.debugLineNum = 282;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_north,_west,_north);
 //BA.debugLineNum = 283;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 284;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_west,_south,_west,_north);
 //BA.debugLineNum = 285;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 }else {
 //BA.debugLineNum = 287;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_west,_south,_west,_north);
 //BA.debugLineNum = 288;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 289;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_south,_west,_south);
 //BA.debugLineNum = 290;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 }else if(_vv3>_east) { 
 //BA.debugLineNum = 293;BA.debugLine="If oY < north Then";
if (_vv4<_north) { 
 //BA.debugLineNum = 294;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_south,_east,_north);
 //BA.debugLineNum = 295;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 296;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_north,_west,_north);
 //BA.debugLineNum = 297;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 }else {
 //BA.debugLineNum = 299;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_south,_west,_south);
 //BA.debugLineNum = 300;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 301;BA.debugLine="poi = getIntersection(oX";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_east,_south,_east,_north);
 //BA.debugLineNum = 302;BA.debugLine="If poi <> Null Then poiL";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 };
 };
 }
};
 //BA.debugLineNum = 308;BA.debugLine="poi = getIntersection(oX, oY,";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_vv5,_vv6,_vv5,_vv0);
 //BA.debugLineNum = 309;BA.debugLine="If poi <> Null Then poiList.Ad";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 310;BA.debugLine="poi = getIntersection(oX, oY,";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_vv5,_vv0,_vv7,_vv0);
 //BA.debugLineNum = 311;BA.debugLine="If poi <> Null Then poiList.Ad";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 312;BA.debugLine="poi = getIntersection(oX, oY,";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_vv7,_vv0,_vv7,_vv6);
 //BA.debugLineNum = 313;BA.debugLine="If poi <> Null Then poiList.Ad";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 314;BA.debugLine="poi = getIntersection(oX, oY,";
_poi = _vvv0(_vv3,_vv4,_cosa,_sina,_vv7,_vv6,_vv5,_vv6);
 //BA.debugLineNum = 315;BA.debugLine="If poi <> Null Then poiList.Ad";
if (_poi!= null) { 
_poilist.Add((Object)(_vvv7(_vv3,_vv4,_poi[(int) (0)],_poi[(int) (1)])));};
 //BA.debugLineNum = 318;BA.debugLine="If poiList.Size > 0 Then";
if (_poilist.getSize()>0) { 
 //BA.debugLineNum = 319;BA.debugLine="poiList.Sort(True)";
_poilist.Sort(anywheresoftware.b4a.keywords.Common.True);
 //BA.debugLineNum = 320;BA.debugLine="dst = poiList.Get(0)";
_dst = (double)(BA.ObjectToNumber(_poilist.Get((int) (0))));
 //BA.debugLineNum = 321;BA.debugLine="new_x = oX + dst * cosA";
_new_x = _vv3+_dst*_cosa;
 //BA.debugLineNum = 322;BA.debugLine="new_y = oY + dst * sinA";
_new_y = _vv4+_dst*_sina;
 //BA.debugLineNum = 323;BA.debugLine="pMap.Put(xyKey(new_x, new_";
_vv1.Put((Object)(_vvvv3(_new_x,_new_y)),(Object)(new double[]{_new_x,_new_y,_a}));
 };
 };
 };
 }
};
 }
};
 //BA.debugLineNum = 332;BA.debugLine="For Each key In pMap.Keys";
{
final anywheresoftware.b4a.BA.IterableList group249 = _vv1.Keys();
final int groupLen249 = group249.getSize()
;int index249 = 0;
;
for (; index249 < groupLen249;index249++){
_key = group249.Get(index249);
 //BA.debugLineNum = 333;BA.debugLine="xya = pMap.Get(key)";
_xya = (double[])(_vv1.Get(_key));
 //BA.debugLineNum = 334;BA.debugLine="x   = xya(X_COORD)";
_x = _xya[_x_coord];
 //BA.debugLineNum = 335;BA.debugLine="y   = xya(Y_COORD)";
_y = _xya[_y_coord];
 //BA.debugLineNum = 336;BA.debugLine="cvs.DrawLine(left + oX, top + oY, left + x";
_v5.DrawLine(_left+_vv3,_top+_vv4,_left+_x,_top+_y,(javafx.scene.paint.Paint)(_vvvvvvv2._vvvv0((int) (2)).getObject()),1);
 //BA.debugLineNum = 337;BA.debugLine="cvs.DrawCircle( _ 	    	left + x, top  + y, _";
_v5.DrawCircle(_left+_x,_top+_y,2,_fx.Colors.Yellow,anywheresoftware.b4a.keywords.Common.False,1);
 }
};
 //BA.debugLineNum = 342;BA.debugLine="cvs.DrawText(\"Raycasting done: \" & (DateTime.N";
_v5.DrawText("Raycasting done: "+BA.NumberToString((anywheresoftware.b4a.keywords.Common.DateTime.getNow()-_vvv1))+" ms",10,20,(javafx.scene.text.Font)(_fx.DefaultFont(12).getObject()),_fx.Colors.Yellow,BA.getEnumFromString(javafx.scene.text.TextAlignment.class,"LEFT"));
 //BA.debugLineNum = 344;BA.debugLine="End Sub";
return "";
}
public static anywheresoftware.b4a.objects.collections.Map  _vvv6(double _x1,double _y1,double _x2,double _y2,double _blocksize,int _maxx,int _maxy) throws Exception{
double _stepx = 0;
double _stepy = 0;
double _dx = 0;
double _dy = 0;
double _absdx = 0;
double _absdy = 0;
double _px = 0;
double _py = 0;
int _bx = 0;
int _by = 0;
anywheresoftware.b4a.objects.collections.Map _blocks = null;
 //BA.debugLineNum = 395;BA.debugLine="Sub getBlocks(x1 As Double, y1 As Double, x2 As Do";
 //BA.debugLineNum = 396;BA.debugLine="Dim stepX, stepY   As Double";
_stepx = 0;
_stepy = 0;
 //BA.debugLineNum = 397;BA.debugLine="Dim dX = (x2 - x1) As Double";
_dx = (_x2-_x1);
 //BA.debugLineNum = 398;BA.debugLine="Dim dY = (y2 - y1) As Double";
_dy = (_y2-_y1);
 //BA.debugLineNum = 400;BA.debugLine="If (x1 = x2) And (y1 = y2) Then";
if ((_x1==_x2) && (_y1==_y2)) { 
 //BA.debugLineNum = 401;BA.debugLine="stepX = 0";
_stepx = 0;
 //BA.debugLineNum = 402;BA.debugLine="stepY = 0";
_stepy = 0;
 }else if((_x1==_x2) && (_y1!=_y2)) { 
 //BA.debugLineNum = 405;BA.debugLine="stepX = 0";
_stepx = 0;
 //BA.debugLineNum = 406;BA.debugLine="stepY = blockSize";
_stepy = _blocksize;
 }else if((_x1!=_x2) && (_y1==_y2)) { 
 //BA.debugLineNum = 409;BA.debugLine="stepX = blockSize";
_stepx = _blocksize;
 //BA.debugLineNum = 410;BA.debugLine="stepY = 0";
_stepy = 0;
 }else {
 //BA.debugLineNum = 413;BA.debugLine="Dim absDx = Abs(dX) As Double";
_absdx = anywheresoftware.b4a.keywords.Common.Abs(_dx);
 //BA.debugLineNum = 414;BA.debugLine="Dim absDy = Abs(dY) As Double";
_absdy = anywheresoftware.b4a.keywords.Common.Abs(_dy);
 //BA.debugLineNum = 415;BA.debugLine="If absDx > absDy Then";
if (_absdx>_absdy) { 
 //BA.debugLineNum = 416;BA.debugLine="stepX = blockSize";
_stepx = _blocksize;
 //BA.debugLineNum = 417;BA.debugLine="stepY = (absDy / absDx) * blockSize";
_stepy = (_absdy/(double)_absdx)*_blocksize;
 }else {
 //BA.debugLineNum = 419;BA.debugLine="stepX = (absDx / absDy) * blockSize";
_stepx = (_absdx/(double)_absdy)*_blocksize;
 //BA.debugLineNum = 420;BA.debugLine="stepY = blockSize";
_stepy = _blocksize;
 };
 };
 //BA.debugLineNum = 423;BA.debugLine="If dX < 0 Then stepX = -stepX";
if (_dx<0) { 
_stepx = -_stepx;};
 //BA.debugLineNum = 424;BA.debugLine="If dY < 0 Then stepY = -stepY";
if (_dy<0) { 
_stepy = -_stepy;};
 //BA.debugLineNum = 426;BA.debugLine="stepX = stepX / 10";
_stepx = _stepx/(double)10;
 //BA.debugLineNum = 427;BA.debugLine="stepY = stepY / 10";
_stepy = _stepy/(double)10;
 //BA.debugLineNum = 429;BA.debugLine="Dim pX = x1 As Double";
_px = _x1;
 //BA.debugLineNum = 430;BA.debugLine="Dim pY = y1 As Double";
_py = _y1;
 //BA.debugLineNum = 431;BA.debugLine="Dim bX, bY As Int";
_bx = 0;
_by = 0;
 //BA.debugLineNum = 432;BA.debugLine="Dim blocks As Map : blocks.Initialize";
_blocks = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 432;BA.debugLine="Dim blocks As Map : blocks.Initialize";
_blocks.Initialize();
 //BA.debugLineNum = 433;BA.debugLine="Do Until Abs(pX - x1) > Abs(dX) Or Abs(pY - y1";
while (!(anywheresoftware.b4a.keywords.Common.Abs(_px-_x1)>anywheresoftware.b4a.keywords.Common.Abs(_dx) || anywheresoftware.b4a.keywords.Common.Abs(_py-_y1)>anywheresoftware.b4a.keywords.Common.Abs(_dy))) {
 //BA.debugLineNum = 434;BA.debugLine="bX = Floor(pX / blockSize)";
_bx = (int) (anywheresoftware.b4a.keywords.Common.Floor(_px/(double)_blocksize));
 //BA.debugLineNum = 435;BA.debugLine="bY = Floor(pY / blockSize)";
_by = (int) (anywheresoftware.b4a.keywords.Common.Floor(_py/(double)_blocksize));
 //BA.debugLineNum = 436;BA.debugLine="If bX < 0 Or bX >= maxX Or bY < 0 Or bY >=";
if (_bx<0 || _bx>=_maxx || _by<0 || _by>=_maxy) { 
if (true) break;};
 //BA.debugLineNum = 437;BA.debugLine="blocks.put(bX & \":\" & bY, Array As Double(";
_blocks.Put((Object)(BA.NumberToString(_bx)+":"+BA.NumberToString(_by)),(Object)(new double[]{_bx,_by}));
 //BA.debugLineNum = 438;BA.debugLine="pX = pX + stepX";
_px = _px+_stepx;
 //BA.debugLineNum = 439;BA.debugLine="pY = pY + stepY";
_py = _py+_stepy;
 }
;
 //BA.debugLineNum = 441;BA.debugLine="bX = Floor(x2 / blockSize)";
_bx = (int) (anywheresoftware.b4a.keywords.Common.Floor(_x2/(double)_blocksize));
 //BA.debugLineNum = 442;BA.debugLine="bY = Floor(y2 / blockSize)";
_by = (int) (anywheresoftware.b4a.keywords.Common.Floor(_y2/(double)_blocksize));
 //BA.debugLineNum = 443;BA.debugLine="blocks.put(bX & \":\" & bY, Array As Double(bX,";
_blocks.Put((Object)(BA.NumberToString(_bx)+":"+BA.NumberToString(_by)),(Object)(new double[]{_bx,_by}));
 //BA.debugLineNum = 444;BA.debugLine="Return blocks";
if (true) return _blocks;
 //BA.debugLineNum = 445;BA.debugLine="End Sub";
return null;
}
public static double  _vvv7(double _x1,double _y1,double _x2,double _y2) throws Exception{
double _dx = 0;
double _dy = 0;
 //BA.debugLineNum = 346;BA.debugLine="Sub getDistance(x1 As Double, y1 As Double, x2 As";
 //BA.debugLineNum = 347;BA.debugLine="Dim dx = x2 - x1 As Double";
_dx = _x2-_x1;
 //BA.debugLineNum = 348;BA.debugLine="Dim dy = y2 - y1 As Double";
_dy = _y2-_y1;
 //BA.debugLineNum = 349;BA.debugLine="Return Sqrt((dx * dx) + (dy * dy))";
if (true) return anywheresoftware.b4a.keywords.Common.Sqrt((_dx*_dx)+(_dy*_dy));
 //BA.debugLineNum = 350;BA.debugLine="End Sub";
return 0;
}
public static double[]  _vvv0(double _x,double _y,double _dx,double _dy,double _x1,double _y1,double _x2,double _y2) throws Exception{
double _r = 0;
double _s = 0;
double _d = 0;
 //BA.debugLineNum = 352;BA.debugLine="Sub getIntersection(x As Double, y As Double, dx A";
 //BA.debugLineNum = 353;BA.debugLine="Dim r, s, d As Double";
_r = 0;
_s = 0;
_d = 0;
 //BA.debugLineNum = 354;BA.debugLine="If (dy / dx <> (y2 - y1) / (x2 - x1)) Then";
if ((_dy/(double)_dx!=(_y2-_y1)/(double)(_x2-_x1))) { 
 //BA.debugLineNum = 355;BA.debugLine="d = ((dx * (y2 - y1)) - dy * (x2 - x1))";
_d = ((_dx*(_y2-_y1))-_dy*(_x2-_x1));
 //BA.debugLineNum = 356;BA.debugLine="If (d <> 0) Then";
if ((_d!=0)) { 
 //BA.debugLineNum = 357;BA.debugLine="r = (((y - y1) * (x2 - x1)) - (x - x1)";
_r = (((_y-_y1)*(_x2-_x1))-(_x-_x1)*(_y2-_y1))/(double)_d;
 //BA.debugLineNum = 358;BA.debugLine="s = (((y - y1) * dx) - (x - x1) * dy)";
_s = (((_y-_y1)*_dx)-(_x-_x1)*_dy)/(double)_d;
 //BA.debugLineNum = 359;BA.debugLine="If (r >= 0 And s >= 0 And s <= 1) Then";
if ((_r>=0 && _s>=0 && _s<=1)) { 
if (true) return new double[]{_x+_r*_dx,_y+_r*_dy};};
 };
 };
 //BA.debugLineNum = 362;BA.debugLine="Return Null";
if (true) return (double[])(anywheresoftware.b4a.keywords.Common.Null);
 //BA.debugLineNum = 363;BA.debugLine="End Sub";
return null;
}
public static String  _initialize(anywheresoftware.b4j.objects.CanvasWrapper _canvas) throws Exception{
 //BA.debugLineNum = 28;BA.debugLine="Sub Initialize(canvas As Canvas)";
 //BA.debugLineNum = 29;BA.debugLine="cvs = canvas";
_v5 = _canvas;
 //BA.debugLineNum = 30;BA.debugLine="lstAngles.Initialize";
_v6.Initialize();
 //BA.debugLineNum = 31;BA.debugLine="lstPoints.Initialize";
_v7.Initialize();
 //BA.debugLineNum = 32;BA.debugLine="angleBuffer.initialize";
_v0.Initialize();
 //BA.debugLineNum = 33;BA.debugLine="pMap.Initialize";
_vv1.Initialize();
 //BA.debugLineNum = 34;BA.debugLine="pFck.Initialize";
_vv2.Initialize();
 //BA.debugLineNum = 35;BA.debugLine="End Sub";
return "";
}
public static boolean  _vvvv1(double _a) throws Exception{
double[] _range = null;
 //BA.debugLineNum = 388;BA.debugLine="Sub isNewAngle(a As Double) As Boolean";
 //BA.debugLineNum = 389;BA.debugLine="For Each range() As Double In lstAngles";
{
final anywheresoftware.b4a.BA.IterableList group1 = _v6;
final int groupLen1 = group1.getSize()
;int index1 = 0;
;
for (; index1 < groupLen1;index1++){
_range = (double[])(group1.Get(index1));
 //BA.debugLineNum = 390;BA.debugLine="If a >= range(0) And a <= range(1) Then";
if (_a>=_range[(int) (0)] && _a<=_range[(int) (1)]) { 
if (true) return anywheresoftware.b4a.keywords.Common.False;};
 }
};
 //BA.debugLineNum = 392;BA.debugLine="Return True";
if (true) return anywheresoftware.b4a.keywords.Common.True;
 //BA.debugLineNum = 393;BA.debugLine="End Sub";
return false;
}
public static String  _process_globals() throws Exception{
 //BA.debugLineNum = 2;BA.debugLine="Sub Process_Globals";
 //BA.debugLineNum = 3;BA.debugLine="Dim Const X_COORD = 0 As Int";
_x_coord = (int) (0);
 //BA.debugLineNum = 4;BA.debugLine="Dim Const Y_COORD = 1 As Int";
_y_coord = (int) (1);
 //BA.debugLineNum = 5;BA.debugLine="Dim Const O_ANGLE = 2 As Int";
_o_angle = (int) (2);
 //BA.debugLineNum = 6;BA.debugLine="Dim const HALF_PI = cPI / 2 As Double";
_half_pi = anywheresoftware.b4a.keywords.Common.cPI/(double)2;
 //BA.debugLineNum = 8;BA.debugLine="Dim cvs As Canvas";
_v5 = new anywheresoftware.b4j.objects.CanvasWrapper();
 //BA.debugLineNum = 10;BA.debugLine="Dim lstAngles   As List";
_v6 = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 11;BA.debugLine="Dim lstPoints   As List";
_v7 = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 12;BA.debugLine="Dim angleBuffer As List";
_v0 = new anywheresoftware.b4a.objects.collections.List();
 //BA.debugLineNum = 14;BA.debugLine="Dim pMap        As Map";
_vv1 = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 15;BA.debugLine="Dim pFck        As Map";
_vv2 = new anywheresoftware.b4a.objects.collections.Map();
 //BA.debugLineNum = 16;BA.debugLine="Dim oX, oY      As Double";
_vv3 = 0;
_vv4 = 0;
 //BA.debugLineNum = 18;BA.debugLine="Dim gMinX       As Double";
_vv5 = 0;
 //BA.debugLineNum = 19;BA.debugLine="Dim gMinY       As Double";
_vv6 = 0;
 //BA.debugLineNum = 20;BA.debugLine="Dim gMaxX       As Double";
_vv7 = 0;
 //BA.debugLineNum = 21;BA.debugLine="Dim gMaxY       As Double";
_vv0 = 0;
 //BA.debugLineNum = 23;BA.debugLine="Dim ts As Long";
_vvv1 = 0L;
 //BA.debugLineNum = 25;BA.debugLine="Type PointAngle(x As Double, y As Double, a As";
;
 //BA.debugLineNum = 26;BA.debugLine="End Sub";
return "";
}
public static String  _vvvv2(double _x,double _y) throws Exception{
 //BA.debugLineNum = 384;BA.debugLine="Sub remPoint(x As Double, y As Double)";
 //BA.debugLineNum = 385;BA.debugLine="pFck.Put(xyKey(x, y), Null)";
_vv2.Put((Object)(_vvvv3(_x,_y)),anywheresoftware.b4a.keywords.Common.Null);
 //BA.debugLineNum = 386;BA.debugLine="End Sub";
return "";
}
public static String  _vvvv3(double _x,double _y) throws Exception{
 //BA.debugLineNum = 369;BA.debugLine="Sub xyKey(x As Double, y As Double) As String";
 //BA.debugLineNum = 370;BA.debugLine="Return x & \",\" & y";
if (true) return BA.NumberToString(_x)+","+BA.NumberToString(_y);
 //BA.debugLineNum = 371;BA.debugLine="End Sub";
return "";
}
}
